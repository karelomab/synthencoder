ProResHQ=" -c:v prores_ks -profile:v 3 "
ProResProxy=" -c:v prores_ks -profile:v 0 "
    
    for synthFiles in *.avs
        do
            echo "Reading .avs file"
            echo "starting HQ FFmpeg encode"
            newName=${synthFiles%.avi.avs}
            ffmpeg.exe -y -loglevel warning -stats -i "$synthFiles" -an -sn -c:v prores_ks -profile:v 3 "$newName""_HQ.mov" ##HQ
            echo "starting proxy FFmpeg encode"
            ffmpeg.exe -y -loglevel warning -stats -i "$newName""_HQ.mov" -an -sn -c:v prores_ks -profile:v 0 "$newName""_Proxy.mov" ##PROXY
            echo "starting next job"
            #rm $synthFiles
        done
    echo "Folder encode complete!"
      