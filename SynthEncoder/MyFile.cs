﻿using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SynthEncoder
{
    public class MyFile
    {
        public string Status { get; set; }
        public string SrcFile {get;set;}
        public string SrcFileNoExtn { get; set; }
        public string SrcDir { get; set; }
        public string AvsFile { get; set; }
        public string ProresHQFile { get; set; }
        public string ProresProxyFile { get; set; }
        public string BatFile { get; set; }
        public List<MyFilter> Filters = new List<MyFilter>();
        public int DurationSeconds { get; set; }
        public string PreviewFileSrc { get; set; }
        public string PreviewFileAVS { get; set; }
        public string PreviewFileSrcBat { get; set; }
        public string PreviewFileAVSBat { get; set; }
        public string PreviewFileFFProbe { get; set; }


        public void ComposeFileAVS()
        {
            StreamWriter fileAVS = new StreamWriter(AvsFile);
            //create avisource object

            //importing libraries/resources
            string[] plugins = Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "resources"));

            foreach (string plug in plugins)
            {
                if (Path.GetExtension(plug) == ".dll")
                {
                    if (Path.GetFileName(plug) == "fft3dfilter.dll")
                    {
                        fileAVS.WriteLine("LoadPlugin(\"" + Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "resources", "LoadDLL64.dll") + "\")");
                        fileAVS.WriteLine("LoadDll(\"" + plug + "\")");
                    }
                    else if (Path.GetFileName(plug) != "LoadDLL64.dll" && Path.GetFileName(plug) != "libfftw3f-3.dll")
                        fileAVS.WriteLine("LoadPlugin(\"" + plug + "\")");

                }
                else if (Path.GetExtension(plug) == ".avsi")
                    fileAVS.WriteLine("Import(\"" + plug + "\")");
            }

            //probe source file
            //ffprobe -v error -select_streams v:0 -show_entries stream=codec_name -of default=noprint_wrappers=1 41375_Edna_Clay_hi8_04.avi
            //output: codec_name=dvvideo

            StreamWriter fileAVSProbe = new StreamWriter(PreviewFileFFProbe);
            fileAVSProbe.WriteLine("ffprobe -v error -select_streams v:0 -show_entries stream=codec_name -of default=noprint_wrappers=1 \"" + SrcFile + "\"");
            fileAVSProbe.Close();
            fileAVSProbe.Dispose();

            ProcessStartInfo pInfo = new ProcessStartInfo()
            {
                FileName = PreviewFileFFProbe,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                CreateNoWindow = true
            };

            Process p = Process.Start(pInfo);
            p.WaitForExit();

            while (!p.StandardOutput.EndOfStream)
            {
                string line = p.StandardOutput.ReadLine();

                //we only care about encoding progress printout
                if (line.Contains("codec_name"))
                    //evaluate codec
                    if (line.Split('=')[1].Equals("dvvideo"))
                        fileAVS.WriteLine("FFVideoSource(\"" + SrcFile + "\")");
                    else if (line.Split('=')[1].Equals("rawvideo"))
                        fileAVS.WriteLine("AVISource(\"" + SrcFile + "\")");
            }

            foreach (MyFilter filter in Filters)
                fileAVS.WriteLine(filter.Args);
            
            fileAVS.Close();
            fileAVS.Dispose();
        }
        public void ComposeFileBat()
        {
            StreamWriter fileBat = new StreamWriter(BatFile);
            fileBat.WriteLine("ECHO EXEC_HQ 1>&2");
            fileBat.WriteLine("ffmpeg -i \"" + AvsFile + "\" -an -sn -c:v prores_ks -profile:v 3 \"" + ProresHQFile + "\"");
            fileBat.WriteLine("ECHO EXEC_PROXY 1>&2");
            fileBat.Write("ffmpeg -i \"" + ProresHQFile + "\" -an -sn -c:v prores_ks -profile:v 0 \"" + ProresProxyFile + "\"");
            fileBat.Close();
            fileBat.Dispose();
        }

        public Image ComposePreviewImageSrc(int seekPercentage)
        {
            int targetSecond = Convert.ToInt32(DurationSeconds * seekPercentage * 0.01);

            if (!File.Exists(PreviewFileSrc))
            {
                StreamWriter fileBat = new StreamWriter(Path.GetTempPath() + Path.GetFileName(SrcFile) + ".bat");
                fileBat.WriteLine("ffmpeg -ss " + TimeSpan.FromSeconds(targetSecond) + " -i \"" + SrcFile + "\" -vframes 1 -vf \"scale = iw / 2:ih / 2\"  -y \"" + PreviewFileSrc + "\"");
                fileBat.Close();
                fileBat.Dispose();

                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    FileName = PreviewFileSrcBat,
                    WorkingDirectory = Application.StartupPath,
                    CreateNoWindow = true
                };

                Process p = Process.Start(startInfo);
                p.WaitForExit();
            }

            if (File.Exists(PreviewFileSrc))
                using (var bmpTemp = new Bitmap(PreviewFileSrc))
                    return new Bitmap(bmpTemp);

            return null;
        }

        public Image ComposePreviewImageAVS(int seekPercentage)
        {
            int targetSecond = Convert.ToInt32(DurationSeconds * seekPercentage * 0.01);

            if (!File.Exists(PreviewFileAVS))
            {
                ComposeFileAVS();

                StreamWriter fileBat = new StreamWriter(Path.GetTempPath() + Path.GetFileName(AvsFile) + ".bat");
                fileBat.WriteLine("ffmpeg -ss " + TimeSpan.FromSeconds(targetSecond) + " -i \"" + AvsFile + "\" -vframes 1 -vf \"scale = iw / 2:ih / 2\" -y \"" + PreviewFileAVS + "\"");
                fileBat.Close();
                fileBat.Dispose();

                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    FileName = PreviewFileAVSBat,
                    WorkingDirectory = Application.StartupPath,
                    CreateNoWindow = true
                };

                Process p = Process.Start(startInfo);
                p.WaitForExit();
            }

            if (File.Exists(PreviewFileAVS))
                using (var bmpTemp = new Bitmap(PreviewFileAVS))
                    return new Bitmap(bmpTemp);

            return null;
        }

        public void DisposePreviewFiles(bool delBat, bool delJpeg)
        {
            if (delBat)
            {
                File.Delete(PreviewFileSrcBat);
                File.Delete(PreviewFileAVSBat);
            }

            if (delJpeg)
            {
                File.Delete(PreviewFileSrc);
                File.Delete(PreviewFileAVS);
                File.Delete(SrcFile + ".ffindex");
            }

            File.Delete(AvsFile);
            File.Delete(PreviewFileFFProbe);
        }

        public void GetTotalSecondsVideo()
        {
            using (var shell = ShellObject.FromParsingName(SrcFile))
            {
                IShellProperty prop = shell.Properties.System.Media.Duration;
                var t = (ulong)prop.ValueAsObject;
                DurationSeconds = Convert.ToInt32(TimeSpan.FromTicks((long)t).TotalSeconds);
            }
        }
    }
}
