﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using System.Drawing.Drawing2D;

namespace SynthEncoder
{
    public partial class WindowImport : Form
    {

        readonly WindowMain wm;
        readonly List<CheckBox> listCheckBoxes = new List<CheckBox>();

        Image buffImage { get; set; }
        Image PreviewImageLeft { get; set; }
        Image PreviewImageRight { get; set; }

        public WindowImport(WindowMain main, List<MyFilter> filters, string[] files)
        {
            InitializeComponent();

            wm = main;

            foreach (string file in files)
                listSourceFiles.Items.Add(file);

            listSourceFiles.SelectedItem = listSourceFiles.Items[0];

            LoadPresets(filters); 
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            List<MyFile> newFiles = new List<MyFile>();

            foreach (string file in listSourceFiles.Items)
            {
                //create MyFile object here
                MyFile myFile = new MyFile();
                myFile.Status = "Waiting";

                //set fields
                myFile.SrcFile = file;
                myFile.SrcFileNoExtn = System.IO.Path.GetFileNameWithoutExtension(myFile.SrcFile);
                myFile.SrcDir = System.IO.Path.GetDirectoryName(myFile.SrcFile);
                myFile.AvsFile = System.IO.Path.Combine(myFile.SrcDir, myFile.SrcFileNoExtn + ".avs");
                myFile.ProresProxyFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(myFile.SrcFile), myFile.SrcFileNoExtn + "_proxy.mov");
                myFile.ProresHQFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(myFile.SrcFile), myFile.SrcFileNoExtn + "_hq.mov");
                myFile.BatFile = Path.Combine(myFile.SrcDir, myFile.SrcFileNoExtn + "_.bat");
                myFile.PreviewFileFFProbe = myFile.AvsFile + "_probe.bat";
                myFile.GetTotalSecondsVideo();

                foreach (CheckBox ctb in listCheckBoxes)
                    if (ctb.Checked)
                        myFile.Filters.Add((MyFilter)ctb.Tag);  //apply this filter

                newFiles.Add(myFile);
            }

            wm.LoadItems(newFiles);
            Close();
        }
        private void LoadPresets(List<MyFilter> filters)
        {
            foreach (MyFilter filter in filters)
            {
                //create new GUI elements here
                CheckBox ctb = new CheckBox();
                ctb.Text = filter.Name + " (" + filter.Type + ")";
                ctb.AutoSize = true;
                ctb.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Regular);

                if (filter.Description.Length > 0)
                {
                    // Create the ToolTip and associate with the Form container.
                    ToolTip toolTip1 = new ToolTip();

                    // Set up the delays for the ToolTip.
                    toolTip1.AutoPopDelay = 5000;
                    toolTip1.InitialDelay = 1000;
                    toolTip1.ReshowDelay = 500;
                    // Force the ToolTip text to be displayed whether or not the form is active.
                    toolTip1.ShowAlways = true;

                    toolTip1.SetToolTip(ctb, filter.Description);
                }

                ctb.Tag = filter;
                flowLayoutPanel1.Controls.Add(ctb);
                listCheckBoxes.Add(ctb);
            }
        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            PreviewImageLeft = null;
            PreviewImageRight = null;
            GeneratePreview();
        }

        private void GeneratePreview()
        {
            if (listSourceFiles.SelectedItem == null)
            {
                MessageBox.Show("Select the file you want to preview from the list first!");
                return;
            }
            
            string fileSource = listSourceFiles.SelectedItem.ToString();

            //create temp MyFile object here
            MyFile myFile = new MyFile();

            //set fields
            myFile.SrcFile = fileSource;
            myFile.SrcFileNoExtn = System.IO.Path.GetFileNameWithoutExtension(myFile.SrcFile);
            myFile.SrcDir = System.IO.Path.GetDirectoryName(myFile.SrcFile);
            myFile.AvsFile = System.IO.Path.Combine(myFile.SrcDir, myFile.SrcFileNoExtn + ".avs");
            myFile.PreviewFileSrc = Path.GetTempPath() + Path.GetFileName(myFile.SrcFile) + ".bmp";
            myFile.PreviewFileAVS = Path.GetTempPath() + Path.GetFileName(myFile.AvsFile) + ".bmp";
            myFile.PreviewFileSrcBat = Path.GetTempPath() + Path.GetFileName(myFile.SrcFile) + ".bat";
            myFile.PreviewFileAVSBat = Path.GetTempPath() + Path.GetFileName(myFile.AvsFile) + ".bat";
            myFile.PreviewFileFFProbe = myFile.AvsFile + "_probe.bat";
            myFile.GetTotalSecondsVideo();

            foreach (CheckBox ctb in listCheckBoxes)
                if (ctb.Checked)
                    myFile.Filters.Add((MyFilter)ctb.Tag);  //apply this filter

            Tag = myFile;

            //cleanup
            myFile.DisposePreviewFiles(true, true);

            DisplayImages(myFile);

            //cleanup
            myFile.DisposePreviewFiles(true, false);
        }

        private void DisplayImages(MyFile myFile)
        {
            if (PreviewImageLeft == null || PreviewImageRight == null)
            {
                pictureBox1.Image = ResizeImage(Image.FromFile("images\\loading.jpg"));
                pictureBox2.Image = ResizeImage(Image.FromFile("images\\loading.jpg"), true);
                PreviewImageLeft = myFile.ComposePreviewImageSrc(trackBar1.Value);
                PreviewImageRight = myFile.ComposePreviewImageAVS(trackBar1.Value);
            }

            if (PreviewImageRight != null)
                pictureBox2.Image = ResizeImage(PreviewImageRight, true);

            if (PreviewImageLeft != null)
                pictureBox1.Image = ResizeImage(PreviewImageLeft);
        }

        private Image ResizeImage(Image imgToResize, bool isCrop=false)
        {
            //Get the image current width  
            int sourceWidth = imgToResize.Width;
            //Get the image current height  
            int sourceHeight = imgToResize.Height;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            //Calulate  width with new desired size  
            nPercentW = ((float)splitContainer2.Width / (float)sourceWidth);
            //Calculate height with new desired size  
            nPercentH = ((float)splitContainer2.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            //New Width  
            int destWidth = splitContainer2.Width;
            //New Height  
            int destHeight = splitContainer2.Height;
            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            // Draw image with new width and height  
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);

            if (isCrop)
            {
                Bitmap bmp = new Bitmap(destWidth, destHeight);
                using (Graphics gr = Graphics.FromImage(bmp))
                {
                    gr.DrawImage((System.Drawing.Image)b, 0, 0, new Rectangle(splitContainer2.SplitterDistance, 0, destWidth, destHeight), GraphicsUnit.Pixel);
                    
                }
                buffImage = (System.Drawing.Image)bmp;
                return (System.Drawing.Image)bmp;
            }

            g.Dispose();

            return (System.Drawing.Image)b;
        }

        private void splitContainer2_SplitterMoved(object sender, SplitterEventArgs e)
        {
            MyFile myFile = (MyFile)Tag;

            if (myFile != null)
                DisplayImages(myFile);
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            MyFile file = (MyFile)Tag;

            if (file != null)
                file.DisposePreviewFiles(true, true);
        }

        private void WindowImport_FormClosing(object sender, FormClosingEventArgs e)
        {
            MyFile file = (MyFile)Tag;

            if (file != null)
                file.DisposePreviewFiles(true, true);
        }
    }
}
