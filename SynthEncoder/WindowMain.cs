﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace SynthEncoder
{
    public partial class WindowMain : Form
    {
        Process processEncoding;
        readonly List<MyFilter> listFilters = new List<MyFilter>();
        ListViewItem curItem;

        public WindowMain()
        {
            InitializeComponent();
            Text = Text + " - " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
            LoadPath();
            LoadFilters();
        }

        #region UTIL
        private static void LoadPath()
        {
            var getEnv = Environment.GetEnvironmentVariable("Path");
            List<string> vars = getEnv.Split(';').ToList();
            bool isRegistered = vars.Contains("SynthEncoder");

            if (!isRegistered)
            {
                vars.Add(Path.Combine(Application.StartupPath, "resources"));
                vars.RemoveAll(x => string.IsNullOrEmpty(x));
                string newValue = String.Join(";", vars.ToArray());
                Environment.SetEnvironmentVariable("Path", newValue);
            }
        }
        private void LoadFilters()
        {
            int index = 2;  //index start at 2 (this is column index in listview, where file is 0 and status is 1)
            List<string> props = new List<string>();
            using (XmlReader reader = XmlReader.Create(Path.Combine(Application.StartupPath, "Filters.xml")))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag  
                        switch (reader.Name.ToString())
                        {
                            case "Name":
                                props.Add(reader.ReadString());
                                break;
                            case "Type":
                                props.Add(reader.ReadString());
                                break;
                            case "Description":
                                props.Add(reader.ReadString());
                                break;
                            case "Args":
                                props.Add(reader.ReadString());

                                listViewData.Columns.Add(props[0]);

                                //make object from props
                                MyFilter filter = new MyFilter();
                                filter.Index = index;
                                filter.Name = props[0];
                                filter.Type = props[1];
                                filter.Description = props[2];
                                filter.Args = props[3];
                                listFilters.Add(filter);
                                props.Clear();
                                index++;
                                break;
                        }
                    }
                }
            }

            for (int i = 2; i < listViewData.Columns.Count; i++)
                listViewData.AutoResizeColumn(i, ColumnHeaderAutoResizeStyle.HeaderSize);
          
        }
        public void LoadItems(List<MyFile> newFiles)
        {
            if (newFiles.Count > 0)
            {
                foreach (MyFile file in newFiles)
                    MakeItem(file);

                btnStart.Enabled = true;
                ToolStripMenuItemStart.Enabled = true;

                listViewData.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.ColumnContent);
            }    
        }
        public void MakeItem(MyFile file)
        {
            ListViewItem lvi = new ListViewItem();
            lvi.Tag = file;
            lvi.Text = file.SrcFile;
            lvi.SubItems.Add(file.Status);

            for (int i = 2; i < listViewData.Columns.Count; i++)
                lvi.SubItems.Add("No");

            foreach (MyFilter filter in file.Filters)
                lvi.SubItems[filter.Index].Text = "Yes";

            listViewData.Items.Add(lvi);
        }
        private void UpdateUIControls(string status)
        {
            switch (status)
            {
                case "Start":
                    UpdatePanelOutput(true);
                    curItem.SubItems[1].Text = "Encoding";

                    btnStart.Enabled = false;
                    ToolStripMenuItemStart.Enabled = false;
                    btnPause.Enabled = true;
                    ToolStripMenuItemPause.Enabled = true;
                    btnStop.Enabled = true;
                    ToolStripMenuItemStop.Enabled = true;

                    RichTextBoxLog("Encoding Started");
                    break;
                case "Pause":
                    curItem.SubItems[1].Text = "Paused";    //update status in listview

                    ToolStripMenuItemPause.Text = "Resume";
                    btnPause.Text = "Resume";

                    RichTextBoxLog("Encoding Paused");
                    break;
                case "Resume":
                    curItem.SubItems[1].Text = "Encoding";    //update status in listview

                    ToolStripMenuItemPause.Text = "Pause";
                    btnPause.Text = "Pause";

                    RichTextBoxLog("Encoding Resumed");
                    break;
                case "Stop":
                    curItem.SubItems[1].Text = "Stopped";

                    btnStart.Enabled = true;
                    ToolStripMenuItemStart.Enabled = true;
                    btnPause.Enabled = false;
                    ToolStripMenuItemPause.Enabled = false;
                    btnStop.Enabled = false;
                    ToolStripMenuItemStop.Enabled = false;
                    break;
            }
        }
        private void UpdatePanelOutput(bool isVisible)
        {
            if (curItem != null)
            {
                MyFile targetFile = (MyFile)curItem.Tag;

                if (targetFile != null)
                {
                    lblOutFilename.Text = targetFile.SrcFile;

                    List<string> sb = new List<string>();

                    foreach (MyFilter f in targetFile.Filters)
                        sb.Add(f.Name);

                    if (sb.Count == 0)
                        lblOutFilters.Text = "None";
                    else lblOutFilters.Text = String.Join(", ", sb.ToArray());

                    lblOutProgress.Text = "0%";
                }
            }

            if (isVisible)
                panelOutputDetails.Visible = true;
            else panelOutputDetails.Visible = false;
        }
        private void RichTextBoxLog(string msg)
        {
            richTextBoxConsoleOut.AppendText("[ " + DateTime.Now + " ] " + msg + Environment.NewLine);
            richTextBoxConsoleOut.ScrollToCaret();
        }
        #endregion

        #region EVENTS
        private void WindowMain_Load(object sender, EventArgs e)
        {
            toolStripMenu.Focus();
        }
        private void ListView1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            WindowImport wi = new WindowImport(this, listFilters, files);
            wi.Show();
        }
        private void ListView1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }
        private void ToolStripMenuItemAddFiles_Click(object sender, EventArgs e)
        {
            ListItemAdd();
        }
        private void ToolStripMenuItemMoveUp_Click(object sender, EventArgs e)
        {
            ListItemMoveUp();
        }
        private void ToolStripMenuItemRemove_Click(object sender, EventArgs e)
        {
            ListItemRemove();
        }
        private void ToolStripMenuItemClear_Click(object sender, EventArgs e)
        {
            ListItemClear();
        }
        private void ToolStripMenuItemStart_Click(object sender, EventArgs e)
        {
            Action("Start");
        }
        private void ToolStripMenuItemPause_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            if (item.Text.Equals("Pause"))
                Action("Pause");
            else if (item.Text.Equals("Resume"))
                Action("Resume");
        }
        private void ToolStripMenuItemStop_Click(object sender, EventArgs e)
        {
            Action("Stop");
        }
        private void BtnStart_Click(object sender, EventArgs e)
        {
            Action("Start");
        }
        private void BtnPause_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Text.Equals("Pause"))
                Action("Pause");
            else if (btn.Text.Equals("Resume"))
                Action("Resume");
        }
        private void BtnStop_Click(object sender, EventArgs e)
        {
            Action("Stop");
        }
        #endregion

        #region HANDLERS
        private void Action(string action)
        {
            switch (action)
            {
                case "Start":
                    ListItemStart();
                    break;
                case "Pause":
                    ListItemPause();
                    UpdateUIControls("Pause");
                    break;
                case "Resume":
                    ListItemResume();
                    UpdateUIControls("Resume");
                    break;
                case "Stop":
                    ListItemStop();
                    UpdateUIControls("Stop");
                    break;
            }
        }
        private void ListItemAdd()
        {
            OpenFileDialog myFileDialog = new OpenFileDialog()
            {
                Title = "Browse avi files",
                Filter = "avi files (*.avi)|*.avi|avs files (*.avs)|*.avs",
                Multiselect = true
            };

            if (myFileDialog.ShowDialog() == DialogResult.OK)
            {
                WindowImport wi = new WindowImport(this, listFilters, myFileDialog.FileNames);
                wi.Show();
            }
        }
        private void ListItemMoveUp()
        {
            foreach (ListViewItem lvi in listViewData.SelectedItems)
            {
                if (lvi.Index > 0)
                {
                    int index = lvi.Index - 1;
                    listViewData.Items.RemoveAt(lvi.Index);
                    listViewData.Items.Insert(index, lvi);
                }
            }
        }
        private void ListItemRemove()
        {
            foreach (ListViewItem lvi in listViewData.SelectedItems)
                listViewData.Items.Remove(lvi);
        }
        private void ListItemClear()
        {
            foreach (ListViewItem lvi in listViewData.Items)
                if (lvi.SubItems[1].Text != "Encoding")
                    lvi.Remove();
        }
        private void ListItemStart()
        {
            //step 0 - identify MyFile that we are going to be processing
            foreach (ListViewItem lvi in listViewData.Items)
            {
                if (lvi.SubItems[1].Text.Equals("Waiting") && processEncoding == null)
                {
                    ProcessEncodingItem(lvi);
                    return;
                }
            }
        }
        private void ListItemPause()
        {
            if (processEncoding != null)
                processEncoding.StandardInput.Write("d");    
        }
        private void ListItemResume()
        {
            if (processEncoding != null)
                processEncoding.StandardInput.WriteLine("");
        }
        private void ListItemStop()
        {
            if (processEncoding != null)
            {
                while (!processEncoding.HasExited)
                {
                    processEncoding.StandardInput.WriteLine("");
                    processEncoding.StandardInput.Write("q");
                    Thread.Sleep(2000);
                }
            }    
        }
        #endregion

        #region PROC
        private void ProcessEncodingItem(ListViewItem lvi)
        {
            MyFile targetFile = (MyFile)lvi.Tag;

            //step 1 - create .avs files if they dont exist
            targetFile.ComposeFileAVS();

            //step 2a - create required .avs and .bat files
            targetFile.ComposeFileBat();

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(TranscodingWorker_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(TranscodingWorker_RunWorkerCompleted);

            curItem = lvi;

            UpdateUIControls("Start");

            worker.RunWorkerAsync(lvi);
        }
        private void TranscodingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ListViewItem lvi = (ListViewItem)e.Argument;
            MyFile targetFile = (MyFile)lvi.Tag;

            ////step 2b execute .bat file
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = targetFile.BatFile,
                WorkingDirectory = Application.StartupPath,
                ErrorDialog = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                CreateNoWindow = true
            };

            processEncoding = Process.Start(startInfo);

            while (!processEncoding.StandardError.EndOfStream)
            {
                string line = processEncoding.StandardError.ReadLine();

                BeginInvoke((MethodInvoker)delegate
                {
                    RichTextBoxLog(line);
                });

                //we only care about encoding progress printout

                if (line.Contains("EXEC_"))
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        lblOutProfile.Text = line.Replace("EXEC_", "");
                    });
                }

                else if (line.Contains("frame="))
                {
                    string[] properties = line.Split(new char[] { ' ', '=' }, StringSplitOptions.RemoveEmptyEntries);
                    TimeSpan curTime;

                    // Use Dictionary as a map.
                    var map = new Dictionary<string, string>();

                    for (int i = 0; i < properties.Length; i += 2)
                    {
                        map.Add(properties[i], properties[i + 1]);
                    }

                    string mapValue;
                    if (map.TryGetValue("time", out mapValue))
                    {
                        curTime = TimeSpan.Parse(mapValue);

                        //calculate time diff
                        double a = Convert.ToInt64(targetFile.DurationSeconds);
                        double b = curTime.TotalSeconds;
                        double c = b / a * 100;

                        BeginInvoke((MethodInvoker)delegate
                        {
                            lblOutProgress.Text = c + "%";
                        });
                    }
                }
            }

            e.Result = lvi; //pass object along
        }
        private void TranscodingWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ListViewItem lvi = (ListViewItem)e.Result;

            if (curItem.SubItems[1].Text != "Stopped")
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    lvi.SubItems[1].Text = "Completed";
                });
            } else
            {
                RichTextBoxLog("Stopped Encoding");
            }

            processEncoding = null;
            curItem = null;
            panelOutputDetails.Visible = false;
            UpdatePanelOutput(false);
            ListItemStart();
        }
        #endregion

    }
}
