﻿
namespace SynthEncoder
{
    partial class WindowMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WindowMain));
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.ToolStripMenuItemAddFiles = new System.Windows.Forms.ToolStripButton();
            this.ToolStripMenuItemMoveUp = new System.Windows.Forms.ToolStripButton();
            this.ToolStripMenuItemRemove = new System.Windows.Forms.ToolStripButton();
            this.ToolStripMenuItemClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolStripMenuItemStart = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemPause = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemStop = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listViewData = new System.Windows.Forms.ListView();
            this.colSrcFile = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.richTextBoxConsoleOut = new System.Windows.Forms.RichTextBox();
            this.panelOutputDetails = new System.Windows.Forms.Panel();
            this.lblOutProfile = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOutProgress = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblOutFilters = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblOutFilename = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.toolStripMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panelOutputDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemAddFiles,
            this.ToolStripMenuItemMoveUp,
            this.ToolStripMenuItemRemove,
            this.ToolStripMenuItemClear,
            this.toolStripSeparator1,
            this.toolStripDropDownButton1});
            this.toolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.Size = new System.Drawing.Size(1382, 25);
            this.toolStripMenu.TabIndex = 1;
            this.toolStripMenu.Text = "toolStrip1";
            // 
            // ToolStripMenuItemAddFiles
            // 
            this.ToolStripMenuItemAddFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ToolStripMenuItemAddFiles.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripMenuItemAddFiles.Image")));
            this.ToolStripMenuItemAddFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripMenuItemAddFiles.Name = "ToolStripMenuItemAddFiles";
            this.ToolStripMenuItemAddFiles.Size = new System.Drawing.Size(59, 22);
            this.ToolStripMenuItemAddFiles.Text = "Add Files";
            this.ToolStripMenuItemAddFiles.Click += new System.EventHandler(this.ToolStripMenuItemAddFiles_Click);
            // 
            // ToolStripMenuItemMoveUp
            // 
            this.ToolStripMenuItemMoveUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ToolStripMenuItemMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripMenuItemMoveUp.Image")));
            this.ToolStripMenuItemMoveUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripMenuItemMoveUp.Name = "ToolStripMenuItemMoveUp";
            this.ToolStripMenuItemMoveUp.Size = new System.Drawing.Size(59, 22);
            this.ToolStripMenuItemMoveUp.Text = "Move Up";
            this.ToolStripMenuItemMoveUp.Click += new System.EventHandler(this.ToolStripMenuItemMoveUp_Click);
            // 
            // ToolStripMenuItemRemove
            // 
            this.ToolStripMenuItemRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ToolStripMenuItemRemove.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripMenuItemRemove.Image")));
            this.ToolStripMenuItemRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripMenuItemRemove.Name = "ToolStripMenuItemRemove";
            this.ToolStripMenuItemRemove.Size = new System.Drawing.Size(54, 22);
            this.ToolStripMenuItemRemove.Text = "Remove";
            this.ToolStripMenuItemRemove.Click += new System.EventHandler(this.ToolStripMenuItemRemove_Click);
            // 
            // ToolStripMenuItemClear
            // 
            this.ToolStripMenuItemClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ToolStripMenuItemClear.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripMenuItemClear.Image")));
            this.ToolStripMenuItemClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripMenuItemClear.Name = "ToolStripMenuItemClear";
            this.ToolStripMenuItemClear.Size = new System.Drawing.Size(38, 22);
            this.ToolStripMenuItemClear.Text = "Clear";
            this.ToolStripMenuItemClear.Click += new System.EventHandler(this.ToolStripMenuItemClear_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemStart,
            this.ToolStripMenuItemPause,
            this.ToolStripMenuItemStop});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(60, 22);
            this.toolStripDropDownButton1.Text = "Actions";
            // 
            // ToolStripMenuItemStart
            // 
            this.ToolStripMenuItemStart.Enabled = false;
            this.ToolStripMenuItemStart.Name = "ToolStripMenuItemStart";
            this.ToolStripMenuItemStart.Size = new System.Drawing.Size(105, 22);
            this.ToolStripMenuItemStart.Text = "Start";
            this.ToolStripMenuItemStart.Click += new System.EventHandler(this.ToolStripMenuItemStart_Click);
            // 
            // ToolStripMenuItemPause
            // 
            this.ToolStripMenuItemPause.Enabled = false;
            this.ToolStripMenuItemPause.Name = "ToolStripMenuItemPause";
            this.ToolStripMenuItemPause.Size = new System.Drawing.Size(105, 22);
            this.ToolStripMenuItemPause.Text = "Pause";
            this.ToolStripMenuItemPause.Click += new System.EventHandler(this.ToolStripMenuItemPause_Click);
            // 
            // ToolStripMenuItemStop
            // 
            this.ToolStripMenuItemStop.Enabled = false;
            this.ToolStripMenuItemStop.Name = "ToolStripMenuItemStop";
            this.ToolStripMenuItemStop.Size = new System.Drawing.Size(105, 22);
            this.ToolStripMenuItemStop.Text = "Stop";
            this.ToolStripMenuItemStop.Click += new System.EventHandler(this.ToolStripMenuItemStop_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.AllowDrop = true;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listViewData);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1382, 594);
            this.splitContainer1.SplitterDistance = 394;
            this.splitContainer1.TabIndex = 3;
            // 
            // listViewData
            // 
            this.listViewData.AllowDrop = true;
            this.listViewData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSrcFile,
            this.colStatus});
            this.listViewData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewData.FullRowSelect = true;
            this.listViewData.GridLines = true;
            this.listViewData.HideSelection = false;
            this.listViewData.Location = new System.Drawing.Point(0, 0);
            this.listViewData.Name = "listViewData";
            this.listViewData.Size = new System.Drawing.Size(1382, 394);
            this.listViewData.TabIndex = 1;
            this.listViewData.UseCompatibleStateImageBehavior = false;
            this.listViewData.View = System.Windows.Forms.View.Details;
            this.listViewData.DragDrop += new System.Windows.Forms.DragEventHandler(this.ListView1_DragDrop);
            this.listViewData.DragEnter += new System.Windows.Forms.DragEventHandler(this.ListView1_DragEnter);
            // 
            // colSrcFile
            // 
            this.colSrcFile.Text = "Source File";
            this.colSrcFile.Width = 320;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            this.colStatus.Width = 75;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer2.Panel1.Controls.Add(this.richTextBoxConsoleOut);
            this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panelOutputDetails);
            this.splitContainer2.Panel2.Controls.Add(this.btnStart);
            this.splitContainer2.Panel2.Controls.Add(this.btnPause);
            this.splitContainer2.Panel2.Controls.Add(this.btnStop);
            this.splitContainer2.Size = new System.Drawing.Size(1382, 196);
            this.splitContainer2.SplitterDistance = 645;
            this.splitContainer2.TabIndex = 0;
            // 
            // richTextBoxConsoleOut
            // 
            this.richTextBoxConsoleOut.BackColor = System.Drawing.Color.White;
            this.richTextBoxConsoleOut.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxConsoleOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxConsoleOut.Location = new System.Drawing.Point(10, 0);
            this.richTextBoxConsoleOut.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.richTextBoxConsoleOut.Name = "richTextBoxConsoleOut";
            this.richTextBoxConsoleOut.ReadOnly = true;
            this.richTextBoxConsoleOut.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBoxConsoleOut.Size = new System.Drawing.Size(635, 196);
            this.richTextBoxConsoleOut.TabIndex = 4;
            this.richTextBoxConsoleOut.Text = "";
            // 
            // panelOutputDetails
            // 
            this.panelOutputDetails.Controls.Add(this.lblOutProfile);
            this.panelOutputDetails.Controls.Add(this.label3);
            this.panelOutputDetails.Controls.Add(this.lblOutProgress);
            this.panelOutputDetails.Controls.Add(this.label6);
            this.panelOutputDetails.Controls.Add(this.lblOutFilters);
            this.panelOutputDetails.Controls.Add(this.label4);
            this.panelOutputDetails.Controls.Add(this.lblOutFilename);
            this.panelOutputDetails.Controls.Add(this.label1);
            this.panelOutputDetails.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelOutputDetails.Location = new System.Drawing.Point(0, 51);
            this.panelOutputDetails.Name = "panelOutputDetails";
            this.panelOutputDetails.Size = new System.Drawing.Size(733, 145);
            this.panelOutputDetails.TabIndex = 0;
            this.panelOutputDetails.Visible = false;
            // 
            // lblOutProfile
            // 
            this.lblOutProfile.AutoSize = true;
            this.lblOutProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblOutProfile.Location = new System.Drawing.Point(123, 44);
            this.lblOutProfile.Name = "lblOutProfile";
            this.lblOutProfile.Size = new System.Drawing.Size(98, 17);
            this.lblOutProfile.TabIndex = 19;
            this.lblOutProfile.Text = "HQ or PROXY";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label3.Location = new System.Drawing.Point(61, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Profile :";
            // 
            // lblOutProgress
            // 
            this.lblOutProgress.AutoSize = true;
            this.lblOutProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblOutProgress.Location = new System.Drawing.Point(123, 104);
            this.lblOutProgress.Name = "lblOutProgress";
            this.lblOutProgress.Size = new System.Drawing.Size(16, 17);
            this.lblOutProgress.TabIndex = 17;
            this.lblOutProgress.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label6.Location = new System.Drawing.Point(40, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Progress : ";
            // 
            // lblOutFilters
            // 
            this.lblOutFilters.AutoSize = true;
            this.lblOutFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblOutFilters.Location = new System.Drawing.Point(123, 74);
            this.lblOutFilters.Name = "lblOutFilters";
            this.lblOutFilters.Size = new System.Drawing.Size(63, 17);
            this.lblOutFilters.TabIndex = 15;
            this.lblOutFilters.Text = "listFilters";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label4.Location = new System.Drawing.Point(8, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Applied Filters : ";
            // 
            // lblOutFilename
            // 
            this.lblOutFilename.AutoSize = true;
            this.lblOutFilename.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblOutFilename.Location = new System.Drawing.Point(123, 14);
            this.lblOutFilename.Name = "lblOutFilename";
            this.lblOutFilename.Size = new System.Drawing.Size(83, 17);
            this.lblOutFilename.TabIndex = 13;
            this.lblOutFilename.Text = "filename.ext";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label1.Location = new System.Drawing.Point(31, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Processing :";
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnStart.Location = new System.Drawing.Point(13, 9);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 31);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // btnPause
            // 
            this.btnPause.Enabled = false;
            this.btnPause.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnPause.Location = new System.Drawing.Point(94, 9);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 31);
            this.btnPause.TabIndex = 10;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.BtnPause_Click);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.btnStop.Location = new System.Drawing.Point(175, 9);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 31);
            this.btnStop.TabIndex = 11;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // WindowMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1382, 619);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStripMenu);
            this.Name = "WindowMain";
            this.Text = "SynthEncoder";
            this.Load += new System.EventHandler(this.WindowMain_Load);
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panelOutputDetails.ResumeLayout(false);
            this.panelOutputDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton ToolStripMenuItemClear;
        private System.Windows.Forms.ToolStripButton ToolStripMenuItemAddFiles;
        private System.Windows.Forms.ToolStripButton ToolStripMenuItemMoveUp;
        private System.Windows.Forms.ToolStripButton ToolStripMenuItemRemove;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemStart;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemPause;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemStop;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.RichTextBox richTextBoxConsoleOut;
        public System.Windows.Forms.ListView listViewData;
        private System.Windows.Forms.ColumnHeader colSrcFile;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.Panel panelOutputDetails;
        private System.Windows.Forms.Label lblOutProgress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblOutFilters;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblOutFilename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblOutProfile;
        private System.Windows.Forms.Label label3;
    }
}

