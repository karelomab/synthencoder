# Application Dependency
In order to use this program you need to have **AviSynthPlus installed**.
Version 3.7.0 was used for writing and testing this application. 

# Download
Download the latest version of SynthEncoder [click here](https://bitbucket.org/karelomab/synthencoder/downloads/SynthEncoder_1003.zip)

Download AviSynth+ 3.7.0 [click here](https://bitbucket.org/karelomab/synthencoder/downloads/AviSynthPlus_3.7.0_20210111_vcredist.exe)

AviSynth+ Releases on Git [click here](https://github.com/AviSynth/AviSynthPlus/releases)

# Project Roadmap
### Features:
- [x] Dynamic Filter definitions
- [x] Process Film files
- [x] Process Video files
- [x] Output Preview
- [x] Transcoding Controls (Start/Pause/Resume/Stop)
- [x] Transcoding Output Details
- [ ] Flexible Output Parameters (currently output is hardcoded to ProRes HQ and ProRes Proxy)

# Changelog
# SynthEncoder 1.0.0.3
- Added 'Loading' images to Preview picturebox for better UX
- Removed 'CheckTextBox' controls and replaced them with just 'CheckBox' controls within Import Window dialog.
- Added Mouse Tooltips displaying description message upon hovering over a CheckBox (filter name)
- Modified the data structure for pre-defined filters stored in 'Filters.xml'

# SynthEncoder 1.0.0.2
- Downgraded from .NET Framework 5 to 4.8 due to external libraries being incompatible with 5
- Redesigned ImportWindow UI: Added output preview feature
- Redesigned MainWindow UI : Panel Left : Added embedded console output window
- Redesigned MainWindow UI : Panel Right : Added Action buttons to Start/Pause/Resume/Stop encoding, Labels to display output data

# SynthEncoder 1.0.0.1
- Removed static filter definitions stored in 'App.config' file
- Added dynamic filter definitions stored in 'Filters.xml'
- Added new item statuses '**Paused**' and '**Stopped**'